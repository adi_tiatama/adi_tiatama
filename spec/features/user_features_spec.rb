require 'spec_helper'

describe 'UserRegisteration' do
  it 'allows a user to Registeration' do
    visit new_user_registration_path
    fill_in 'Email', with: "1111@kk.com"
    fill_in 'Password', with: "1111"
    fill_in 'Password confirmation', with: "1111"
    click_button 'Sign Up'
    #visit '/phones'
    page.should have_content "Welcome! You have signed up successfully."
  end
end

describe 'UserSignIn' do
  let(:user) {FactoryGirl.create(:user)}
  it 'should allow a registered user to sign in' do
    visit new_user_session_path
    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    click_button 'Sign In'
    #visit '/phones'
    page.should have_content 'Signed in successfully.'
  end
  it 'should not allow an unregistered user to sign in' do
    visit new_user_session_path
    fill_in 'Email', with: "error@error.com"
    fill_in 'Password', with: "errorpassword"
    click_button 'Sign In'
    #visit new_user_session_path
    page.should have_content 'Sign In Invalid email or password.'
  end
end