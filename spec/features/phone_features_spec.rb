require 'spec_helper'

describe "Phones" do
  before(:each) do
    user = FactoryGirl.create(:user)
    visit new_user_session_path
    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    # binding.pry
    click_button 'Sign In'
    # login_as(user, :scope => :user)
  end

  context "Rspec with Capybara"  do
    it 'Gooooo.... action!', :js => true do
    # it 'insert new phone, Ok!' do
      #insert phone
      fill_in "phone_name", with: "adit"
      fill_in "phone_nomer", with: "0857123456789"
      click_button "add phone"
      page.should have_content "0857123456789"
      page.should have_content "adit"

      #edit phone
      click_link 'edit'
      fill_in "phone_name", with: "aditedit"
      click_button "save phone"
      page.should have_content "aditedit"

      #open phonelist
      click_link "aditedit"
      page.should have_content "aditedit"

      #add phonelist
      fill_in "phonelist_namelist", with: "aditlist"
      fill_in "phonelist_nomerlist", with: "0857123456789list"
      click_button "add phonelist"
      page.should have_content "aditlist"
      page.should have_content "0857123456789list"

      #edit phonelist
      click_link 'edit'
      fill_in "phonelist_namelist", with: "aditlistaditlist"
      click_button "save phonelist"
      page.should have_content "aditlistaditlist"

      #delete phonelist
      click_link 'delete'
      alert = page.driver.browser.switch_to.alert
      alert.accept
      click_link '<< back'

      #delete phone
      click_link 'delete'
      alert = page.driver.browser.switch_to.alert
      alert.accept

      #insert phone
      fill_in "phone_name", with: "adit new"
      fill_in "phone_nomer", with: "0857123456789"
      click_button "add phone"
      page.should have_content "0857123456789"
      page.should have_content "adit new"

      #insert phone
      fill_in "phone_name", with: "adit1 new"
      fill_in "phone_nomer", with: "0857987654321"
      click_button "add phone"
      page.should have_content "0857987654321"
      page.should have_content "adit1 new"

      click_link 'Account'
      click_link 'SignOut'
      click_link 'SignIn'

      #sign in Administrator
      user_admin = FactoryGirl.create(:user_admin)
      visit new_user_session_path
      fill_in 'Email', with: user_admin.email
      fill_in 'Password', with: user_admin.password
      click_button 'Sign In'

      #edit account role
      click_link 'Account'
      find(:xpath, "//a[@href='/accounts/1/edit']").click
      choose("account_role_administrator")
      click_button("save administration")
      click_link 'Account'
      click_link 'Home'

      click_link 'SignOut'
    end
  end
end