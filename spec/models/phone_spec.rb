# # spec/models/phone_spec.rb
require 'spec_helper'

describe Phone do
	it "1. valid factory" do
		FactoryGirl.create(:phone).should be_valid
	end
	it "2. invalid without name" do
		FactoryGirl.build(:phone, name:nil).should_not be_valid	
	end
	it "3. invalid without nomer" do
		FactoryGirl.build(:phone, nomer:nil).should_not be_valid	
	end
	it "4. invalid without name & nomer" do
		FactoryGirl.build(:phone, name:nil, nomer:nil).should_not be_valid	
	end
	it "5. does not valid, duplicates phone number" do
		FactoryGirl.create(:phone, name: "john", nomer: "1234567890")
		FactoryGirl.build(:phone, name: "john", nomer: "1234567890").should_not be_valid
	end
end