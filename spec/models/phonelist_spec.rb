# spec/models/phonelist_spec.rb
require 'spec_helper'

describe Phonelist do
	it "1. has an phone" do
		phone = FactoryGirl.create :phone
		FactoryGirl.create(:phonelist, phone_id: phone.id).should be_valid
	end
	it "2. has an phone invalid without name" do
		phone = FactoryGirl.create :phone
		FactoryGirl.build(:phonelist, namelist:nil, phone_id: phone.id).should_not be_valid
	end
	it "3. has an phone invalid without nomer" do
		phone = FactoryGirl.create :phone
		FactoryGirl.build(:phonelist, nomerlist:nil, phone_id: phone.id).should_not be_valid
	end
	it "4. has an phone invalid without name & nomer" do
		phone = FactoryGirl.create :phone
		FactoryGirl.build(:phonelist, namelist:nil, nomerlist:nil, phone_id: phone.id).should_not be_valid
	end
	it "5. has an phone manual" do
		phone = FactoryGirl.create :phone
		phonelist = Phonelist.new(namelist: "teslist", nomerlist: "12345list", phone_id: phone.id)
		#phonelist.save.should == true
		#phonelist.phone_id.should == phone.id
		expect(phonelist.save).to be_true
		expect(phonelist.phone_id).to eql phone.id
	end
	it "6. has an phone, does not valid - duplicates phone number" do
		phone = FactoryGirl.create :phone
		FactoryGirl.create(:phonelist, namelist: "jon", nomerlist: "123456", phone_id: phone.id)
		FactoryGirl.build(:phonelist, namelist: "jin", nomerlist: "123456", phone_id: phone.id).should_not be_valid
	end
end

# describe Phonelist do
# 	FactoryGirl.create(:phonelist)
# end