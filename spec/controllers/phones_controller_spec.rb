require 'spec_helper'

describe PhonesController do

before(:each) do
	sign_in FactoryGirl.create(:user)
end


	describe "GET #index" do
		it "get an array of phones" do
			phone = FactoryGirl.create(:phone)
			get :index
			expect(assigns(:phones)).to eq([phone])
		end
		it "has a 200 status code" do
			get :index
			#response.should render_template :index
			expect(response.status).to eq(200)
		end
	end


	describe "GET #show" do
		it "assigns the requested phone to @phone" do
			phone = FactoryGirl.create(:phone)
			get :show, id: phone
			#expect(assigns(:phone)).to eq([phone])
			#assigns(:phone).attributes.should eq(phone.attributes)
			assigns(:phone).should == phone
		end
		it "has a 200 status code" do
			get :show, id: FactoryGirl.create(:phone)
			expect(response.status).to eq(200)
		end
	end


	describe "POST #create" do
		context "with valid attributes" do
			it "create a new phone" do
				expect{
					post :create, phone: FactoryGirl.attributes_for(:phone)
				}.to change(Phone,:count).by(1)
			end
			it "redirect to new phone" do
				post :create, phone: FactoryGirl.attributes_for(:phone)
				response.should redirect_to Phone.last
			end
		end
		context "with invalid attributes" do
			it "create a new phone" do
				expect{
					post :create, phone: FactoryGirl.attributes_for(:invalid_phone)
				}.to_not change(Phone,:count)
			end
			it "re-renders new method" do
				post :create, phone: FactoryGirl.attributes_for(:invalid_phone)
				response.should render_template :new
			end
		end
	end


	describe "PUT #update" do
	  before(:each) do
	  	@phone = FactoryGirl.create(:phone, name: "adi_tiatama", nomer: "0857123456789")
	  end
		context "with valid attributes" do
			it "located the requested @phone" do
				put :update, id: @phone, phone: FactoryGirl.attributes_for(:phone)
				assigns(:phone).should eq(@phone)
			end
			it "changes @phone's attributes" do
				put :update, id: @phone,
					phone: FactoryGirl.attributes_for(:phone, name: "adit", nomer: "0857123456789")
				@phone.reload
				@phone.name.should eq("adit")
				@phone.nomer.should eq("0857123456789")
			end
			it "renders to index method" do
				put :update, id: @phone, phone: FactoryGirl.attributes_for(:phone)
				response.should render_template :index
			end
		end
		context "with invalid attributes" do
			it "located the requested @phone" do
				put :update, id: @phone, phone: FactoryGirl.attributes_for(:invalid_phone)
				assigns(:phone).should eq(@phone)
			end
			it "does not change @phone's attributes" do
				put :update, id: @phone,
					phone: FactoryGirl.attributes_for(:phone, name: "adit", nomer: nil)
				@phone.reload
				@phone.name.should_not eq("adit")
				@phone.nomer.should eq("0857123456789")
			end
			it "re-renders to edit method" do
				put :update, id: @phone, phone: FactoryGirl.attributes_for(:invalid_phone)
				response.should render_template :edit
			end
		end
	end


	describe "DELETE #destroy" do
	  before(:each) do
	  	@phone = FactoryGirl.create(:phone)
	  end
		it "deletes phone" do
			expect{
				delete :destroy, id: @phone
			}.to change(Phone, :count).by(-1)
		end
		it "redirects to phone#index" do
			delete :destroy, id: @phone
			response.should redirect_to phones_url
		end
	end

end