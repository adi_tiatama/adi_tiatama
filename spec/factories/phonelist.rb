#spec/factories/phonelist.rb
require 'faker'
#require "spec_helper"

FactoryGirl.define do
	factory :phonelist do |f|
		f.namelist {Faker::Name.name}
		f.nomerlist {Faker::PhoneNumber.phone_number}
	end
	factory :invalid_phonelist, parent: :phonelist do |f|
		f.namelist nil
	end
end


# FactoryGirl.define do
# 	factory :phone do
# 		name 'tess'
# 		nomer '123456'
# 	end

# 	factory :phonelist do
# 		namelist 'tesslist'
# 		nomerlist '123456list'
# 		phone
# 	end
# end

# FactoryGirl.define do
# 		factory :phone_with_phonelists, :parent => :phone do
# 			after_create do |phone|
# 				FactoryGirl.create(:phonelist, :phone => phone)
# 			end
# 		end
# end

# FactoryGirl.define do
# 	factory :phone do
# 		name 'tttt'
# 		nomer '987654'
# 		after_build do |phone|
# 			phone.phonelists << FactoryGirl.build(:phonelist, :phone => phone)
# 		end
# 	end
# end

# FactoryGirl.create(:phonelist)