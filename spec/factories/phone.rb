# #spec/factories/phone.rb
require 'faker'

FactoryGirl.define do
	factory :phone do |f|
		f.name {Faker::Name.name}
		f.nomer {Faker::PhoneNumber.phone_number}
		f.user_id "1"
	end
	factory :invalid_phone, parent: :phone do |f|
		f.name nil
	end
end