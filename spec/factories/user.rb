require 'faker'

FactoryGirl.define do
	factory :user do |f|
		# f.email {Faker::Internet.email}
		# f.password {Faker::Name.name}
    f.email "user@dwp.co.id"
    f.password "user"
    f.role "User"
	end
  factory :user_admin, parent: :user do |f|
    f.email "admin@dwp.co.id"
    f.password "admin"
    f.role "Administrator"
  end
end