class PhonesController < ApplicationController
   load_and_authorize_resource

    def index
      # if current_user.role == "Administrator"
        @phones = Phone.all.order("name ASC")
      # else
      #   @phones = Phone.all.order("name ASC").where(user_id: current_user.id)
      # end
    end

    def new
       @phone = Phone.new
    end

    def create
       @phone = Phone.new(phone_params)
       # if current_user.role == "Administrator"
        @phones = Phone.all.order("name ASC")
       # else
       #  @phones = Phone.all.order("name ASC").where(user_id: current_user.id)
       # end

       respond_to do |format|
        if @phone.save
          format.html { redirect_to @phone, notice: "Phone created" }
          format.js
        else
          format.html {render 'new'}
          format.js
        end
       end
    end

   def show
       @phone = Phone.find(params[:id])
   end

   def destroy
       @phone = Phone.find(params[:id])
       @phone.destroy

       redirect_to @phone
   end

   def edit
       @phone = Phone.find(params[:id])
   end

   def update
       @phone = Phone.find(params[:id])
       # if current_user.role == "Administrator"
        @phones = Phone.all.order("name ASC")
       # else
       #  @phones = Phone.all.order("name ASC").where(user_id: current_user.id)
       # end
       if @phone.update(params[:phone].permit(:name, :nomer))
           render 'index'
       else
           render 'edit'
       end
   end

   private
    def phone_params
      params.require(:phone).permit(:name, :nomer, :user_id)
   end
 end
