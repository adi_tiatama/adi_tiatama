class AccountsController < ApplicationController


  def index
    @accounts = User.all
  end

  def edit
    @account = User.find(params[:id])
    authorize! :edit, @account
  end

  def update
    authorize! :update, @account
    @account = User.find(params[:id])
    if @account.update(params[:account].permit(:role))
      redirect_to accounts_url
    else
      render 'edit'
    end
  end

  def show
   @account = User.find(params[:id])
 end

 def destroy
  @account = User.find(params[:id])
  authorize! :destroy, @account
  @account.destroy

  redirect_to accounts_url
end
end
