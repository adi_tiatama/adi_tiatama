require 'spec_helper'

#class PhoneControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end
  describe PhonesController do
  describe "GET #index" do
    it "responds successfully with an HTTP 200 status code" do
      get :index
      expect(response).to be_success
      expect(response.status).to eq(200)
    end

    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
    end

    it "loads all of the posts into @phones" do
      post1, post2 = Phone.create!, Phone.create!
      get :index

      expect(assigns(:phones)).to match_array([post1, post2])
    end

  end
  end
#end