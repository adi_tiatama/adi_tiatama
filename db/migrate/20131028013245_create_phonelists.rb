class CreatePhonelists < ActiveRecord::Migration
  def change
    create_table :phonelists do |t|
      t.string :namelist
      t.string :nomerlist
      t.references :phone

      t.timestamps
    end
 
  	add_index :phonelists, :phone_id
  end
end
